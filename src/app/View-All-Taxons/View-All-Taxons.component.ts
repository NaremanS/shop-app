import { Component, OnInit } from '@angular/core';
import { GetAllTaxonsService } from '../../assets/services/Get-all-taxons.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../assets/services/login.service';
import { GetTaxonProductsService } from '../../assets/services/Get-taxon-products.service';
import { Router } from '@angular/router';
import { GetSingleProductService } from 'src/assets/services/Get-single-product.service';
import { LoginCheckerService } from 'src/assets/services/login-checker.service';
import { PersistanceService } from 'src/assets/services/localstorage.service';


@Component({
  selector: 'app-View-All-Taxons',
  templateUrl: './View-All-Taxons.component.html',
  styleUrls: ['./View-All-Taxons.component.css']
})
export class ViewAllTaxonsComponent implements OnInit {


  loginForm: FormGroup;
  submitted = false;
  accesstoken;
  category = '';
  productsTaxon;
  products = [];
  get_products_res;
  page;
  loading = false;
  logindata;
  loggedin;
  constructor(private getAllTaxonsService: GetAllTaxonsService,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private getTaxonProductsService: GetTaxonProductsService,
    private router: Router,
    private persistanceService: PersistanceService,
    private loginCheckerService: LoginCheckerService) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

  }

  get f() { return this.loginForm.controls; }

  submit() {
    this.submitted = true;
    this.loading = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loginService.login(this.loginForm).subscribe(
      (data) => {
        this.logindata = data;
        this.accesstoken = data['response'].accessToken.access_token;
        this.loginCheckerService.accesstoken = data['response'].accessToken.access_token;
        this.loginCheckerService.loggedin = true;
        this.loading = false;
        this.page = 1;
        this.loggedin = true;
        this.persistanceService.set("user", { token: this.accesstoken });
        console.log(this.persistanceService.get("user"));
        console.log(this.accesstoken);
        console.log(data);
        this.getTaxonsproducts(this.page);
        this.getall(this.accesstoken);
      },
      (error) => { this.loading = false; console.log(error) }
    );
  }

  getall(token) {
    let accesstoken = 'Bearer ' + token;
    this.getAllTaxonsService.GetAllTaxons(token).subscribe(
      (data) => {
        this.productsTaxon = data['_embedded']['items'];
        console.log(this.productsTaxon);
      },
      (error) => { console.log(error); console.log(accesstoken); }
    );
  }

  getTaxonsproducts(page) {
    this.loading = true;
    this.getTaxonProductsService.GetTaxonsproducts(this.accesstoken, page).subscribe(
      (data) => {
        this.loading = false;
        this.products = this.products.concat(data['_embedded']['items']);
        this.get_products_res = data;
        console.log(data);
      },
      (error) => { console.log(error); this.loading = false; }
    );
  }

  navtoggle(item) {
    this.category = item;
  }

  onScroll() {
    if (this.page < this.get_products_res['pages']) {
      this.page++;
      this.getTaxonsproducts(this.page);
    }
  }

  viewDetails(productname, productimg) {
    this.router.navigate(['/details', { productinfo: productname, img: productimg }]);
  }
}
