import { Component, OnInit } from '@angular/core';
import { Product } from '../../assets/product';
import { CreateCartService } from 'src/assets/services/Create-cart.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  customer_orders: Product[] = [];
  
  constructor(private createCartService:CreateCartService,
    private router: Router) { }

  ngOnInit() {
  }
  ngDoCheck(){
    this.customer_orders = this.createCartService.products;
    console.log(this.customer_orders);
  }
  removeproduct(productname, index){
    this.customer_orders.splice(index, 1);
  }
  checkout(){
 this.router.navigate(['customer'])
  }
}
