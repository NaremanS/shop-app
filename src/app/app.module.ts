import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ViewAllTaxonsComponent } from './View-All-Taxons/View-All-Taxons.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GetAllTaxonsService } from '../assets/services/Get-all-taxons.service';
import { TaxonsApiService } from '../assets/services/Taxons-api.service';
import { LoginService } from '../assets/services/login.service';
import { GetTaxonProductsService } from '../assets/services/Get-taxon-products.service';
import { GetSingleProductService } from '../assets/services/Get-single-product.service';
import { LoadingIconComponent } from './loading-icon/loading-icon.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CartComponent } from './cart/cart.component';
import { HeaderComponent } from './header/header.component';
import { MatCardModule, MatSnackBarModule } from '@angular/material';
import { CreateCartService } from '../assets/services/Create-cart.service';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { PersistanceService } from 'src/assets/services/localstorage.service';


@NgModule({
   declarations: [
      AppComponent,
      ProductDetailsComponent,
      ViewAllTaxonsComponent,
      LoadingIconComponent,
      CartComponent,
      HeaderComponent,
      CustomerInfoComponent,
      ThankyouComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      InfiniteScrollModule,
      MatSnackBarModule,
      MatCardModule
   ],
   providers: [
      GetAllTaxonsService,
      TaxonsApiService,
      LoginService,
      GetTaxonProductsService,
      GetSingleProductService,
      CreateCartService,
      PersistanceService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
