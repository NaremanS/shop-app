import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewAllTaxonsComponent } from './View-All-Taxons/View-All-Taxons.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CartComponent } from './cart/cart.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { ThankyouComponent } from './thankyou/thankyou.component';


const routes: Routes = [
  { path: 'orderfinished', component: ThankyouComponent },
  { path: 'customer', component: CustomerInfoComponent },
  { path: 'cart', component: CartComponent },
  { path: 'details', component: ProductDetailsComponent},
  { path: 'home', component: ViewAllTaxonsComponent },
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
