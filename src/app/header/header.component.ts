import { Component, OnInit } from '@angular/core';
import { LoginCheckerService } from 'src/assets/services/login-checker.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedIn= false;
  constructor(private loginCheckerService: LoginCheckerService) { }

  ngOnInit() {
  }
ngDoCheck(){
  if(this.loginCheckerService.accesstoken != null){
    this.isLoggedIn = true;
  }
}
}
