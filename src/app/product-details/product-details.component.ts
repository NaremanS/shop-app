import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { CreateCartService } from 'src/assets/services/Create-cart.service';
import { Product } from 'src/assets/product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product;
  product_img;
  producttocart: Product;
  totalcartvalue = 0;
  value;
  conditionToDisaply=false
  orders = []
  hidden = true;
  selectedSize;
  constructor(private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private createCartService: CreateCartService) { }

  ngOnInit() {
    this.product = this.route.snapshot.params['productinfo'];
    this.product_img = this.route.snapshot.params['img'];
  }

  ChangingSizeValue(e){
    console.log(e.srcElement.value);
    this.selectedSize = e.srcElement.value;
  }
  addToCart(product, productImg){
    this.hidden = false;
    this.producttocart = {name: product, img: productImg, size: this.selectedSize, price: 260}
    this.createCartService.products.push(this.producttocart)
    console.log('addtocart'+ this.createCartService.products);
  }

}
