import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.css']
})
export class CustomerInfoComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router) { }
  customerForm: FormGroup;
  submitted= false;
  ngOnInit() {
    this.customerForm = this.formBuilder.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      gender: ['m', [Validators.required]]
    });
  }

  get f() { return this.customerForm.controls; }

 submit(){
 this.submitted = true;
 if (this.customerForm.invalid) {
  return;
}
this.router.navigate(['orderfinished']);
 }
}
