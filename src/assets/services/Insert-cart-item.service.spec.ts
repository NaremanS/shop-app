/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InsertCartItemService } from './Insert-cart-item.service';

describe('Service: InsertCartItem', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InsertCartItemService]
    });
  });

  it('should ...', inject([InsertCartItemService], (service: InsertCartItemService) => {
    expect(service).toBeTruthy();
  }));
});
