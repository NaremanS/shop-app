/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CreateCustomerService } from './Create-customer.service';

describe('Service: CreateCustomer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateCustomerService]
    });
  });

  it('should ...', inject([CreateCustomerService], (service: CreateCustomerService) => {
    expect(service).toBeTruthy();
  }));
});
