/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductsFilterService } from './products-filter.service';

describe('Service: ProductsFilter', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsFilterService]
    });
  });

  it('should ...', inject([ProductsFilterService], (service: ProductsFilterService) => {
    expect(service).toBeTruthy();
  }));
});
