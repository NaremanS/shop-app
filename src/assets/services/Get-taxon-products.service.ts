import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class GetTaxonProductsService {

  constructor(private http: HttpClient) { }

  GetTaxonsproducts(token, page) {
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': 'Bearer ' + token}),
      params: new HttpParams().set('channelCode','WEB').set('page', page)
    };
    return this.http.get('/productsapi', httpOptions);
  }
}
