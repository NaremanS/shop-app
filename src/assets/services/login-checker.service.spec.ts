/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoginCheckerService } from './login-checker.service';

describe('Service: LoginChecker', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginCheckerService]
    });
  });

  it('should ...', inject([LoginCheckerService], (service: LoginCheckerService) => {
    expect(service).toBeTruthy();
  }));
});
