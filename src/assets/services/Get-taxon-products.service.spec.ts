/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetTaxonProductsService } from './Get-taxon-products.service';

describe('Service: GetTaxonProducts', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetTaxonProductsService]
    });
  });

  it('should ...', inject([GetTaxonProductsService], (service: GetTaxonProductsService) => {
    expect(service).toBeTruthy();
  }));
});
