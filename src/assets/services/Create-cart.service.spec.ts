/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CreateCartService } from './Create-cart.service';

describe('Service: CreateCart', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateCartService]
    });
  });

  it('should ...', inject([CreateCartService], (service: CreateCartService) => {
    expect(service).toBeTruthy();
  }));
});
