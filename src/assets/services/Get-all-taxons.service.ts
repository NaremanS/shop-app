import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TaxonsApiService } from './Taxons-api.service';

@Injectable()
export class GetAllTaxonsService {

constructor(private http: HttpClient,
        private taxonsApiService: TaxonsApiService) { }

    TaxonsUrl = this.taxonsApiService.taxonsApiUrl + '/taxons?channelCode=WEB';

    // accesstoken: string;
    GetAllTaxons(token) {
        // this.accesstoken = 'Bearer ' + token;
  
  const httpOptions = {
      headers: new HttpHeaders({
        'Authorization':'Bearer '+ token
      }),
    };

        return this.http.get('/api',httpOptions);
    }
}
