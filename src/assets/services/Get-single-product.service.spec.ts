/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetSingleProductService } from './Get-single-product.service';

describe('Service: GetSingleProduct', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetSingleProductService]
    });
  });

  it('should ...', inject([GetSingleProductService], (service: GetSingleProductService) => {
    expect(service).toBeTruthy();
  }));
});
