import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class GetSingleProductService {

    constructor(private http: HttpClient) { }
    getProductDetails(token ,productCode) {
        console.log('single product service');
        console.log(productCode);
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer '+token
            })
            // params: new HttpParams().set('channelCode', 'WEB')
        };
        return this.http.get('/getsingleproduct' + '/' + {productCode}, httpOptions);
    }
}
