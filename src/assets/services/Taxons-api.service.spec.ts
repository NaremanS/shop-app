/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TaxonsApiService } from './Taxons-api.service';

describe('Service: TaxonsApi', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxonsApiService]
    });
  });

  it('should ...', inject([TaxonsApiService], (service: TaxonsApiService) => {
    expect(service).toBeTruthy();
  }));
});
