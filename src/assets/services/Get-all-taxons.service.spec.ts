/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetAllTaxonsService } from './Get-all-taxons.service';

describe('Service: GetAllTaxons', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetAllTaxonsService]
    });
  });

  it('should ...', inject([GetAllTaxonsService], (service: GetAllTaxonsService) => {
    expect(service).toBeTruthy();
  }));
});
