import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { TaxonsApiService } from './Taxons-api.service';
@Injectable()
export class LoginService {

    constructor(private http: HttpClient,
        private taxonsApiService: TaxonsApiService) { }

    loginUrl = 'http://register.zvendo.online/api/v3/stores/login';
    login(form) {
        const params = new HttpParams()
        .set('channel','WEB')
        .set('hostName','demo.zvendo.online')
        .set('password',form.controls.password.value)
        .set('username',form.controls.email.value)
        return this.http.post(this.loginUrl,params);
    }
}
